#use wml::debian::template title="Redenen om Debian te kiezen"
#use wml::debian::translation-check translation="abf914830a83b7a9d685e4fc64311723cbb68ae0"
#include "$(ENGLISHDIR)/releases/info"

# Translator:       $Author$
# Translation date: $Date$

<p>Wij zijn blij dat u overweegt om Debian op uw machine te
gaan draaien. Als u nog niet geheel overtuigd bent waarom u Debian zou
proberen, overweeg dan het volgende eens:</p>

<dl>
<dt><strong>Het wordt onderhouden door haar gebruikers.</strong></dt>

    <dd>Als er iets moet worden gerepareerd of verbeterd, dan doen we
    dat gewoon.</dd>

<dt><strong>Ongeëvenaarde ondersteuning</strong></dt>

    <dd>Mail naar de <a href="$(HOME)/MailingLists/">mailinglijsten</a> wordt
    vaak al binnen 15 minuten (of sneller) beantwoord, gratis en door de
    mensen die het systeem hebben ontwikkeld. Vergelijk dat eens met een
    gemiddelde telefonische helpdesk: vaak bent u tegen hoge telefoontarieven
    uren bezig om iemand aan de lijn
    te krijgen en als u dan eenmaal iemand te spreken krijgt kent deze
    vaak het systeem niet goed genoeg om uw vraag te begrijpen.</dd>

<dt><strong>U staat niet alleen met uw keuze</strong></dt>

    <dd>Een breed spectrum van organisaties en individuen gebruikt Debian.
    Op de <a href="$(HOME)/users">Wie gebruikt er Debian?</a> pagina vindt u
    een beschrijving van enkele bekende Debian gebruikers die een korte
    beschrijving wilden geven over hoe en waarom ze Debian gebruiken.

<dt><strong>Het beste verpakkingssysteem ter wereld.</strong></dt>

    <dd>Wordt u ook moe van bestanden die afkomstig zijn van drie versies
    oude software versies en die uw systeem
    vervuilen? Of van het installeren van een softwarepakket dat
    vervolgens uw systeem laat crashen door een softwareconflict? Dpkg,
    Debian’s degelijk verpakkingssysteem, regelt dit alles voor u.</dd>

<dt><strong>Gemakkelijke installatie</strong></dt>

    <dd>Als u denkt dat Debian moeilijk te installeren is, dan heeft u Debian
    de laatste tijd niet geprobeerd. We zijn voortdurend bezig ons
    installatieproces te verbeteren. U kunt de installatie direct doen vanaf
    cd, dvd, Blu-ray, usb-stick of zelfs over het netwerk.</dd>

<dt><strong>Een ongelooflijke hoeveelheid software</strong></dt>

	<dd>Debian wordt geleverd met meer dan <a
	href="$(HOME)/distrib/packages"><packages_in_stable>&nbsp;verschillende
	softwarepakketten</a>. Alles is <a href="free">vrije software</a>. Als u
	gepatenteerde software heeft die onder GNU/Linux of GNU/kFreeBSD draait,
	kunt u dat nog steeds gebruiken - sterker nog, mogelijk
	heeft Debian zelfs een installatieprogramma dat alles
	voor u automatisch installeert en configureert.</dd>

<dt><strong>Pakketten zijn goed geïntegreerd</strong></dt>

    <dd>Debian doet het beter dan alle andere distributies wat betreft
    hoe goed de verschillende pakketten zijn geïntegreerd. Omdat
    alle software door een coherente groep wordt verpakt, kunt u niet
    alleen alle pakketten op een plaats vinden, maar kunt u er
    ook zeker van zijn dat we alle gecompliceerde onderlinge
    afhankelijkheden tussen de verschillende pakketten goed hebben
    geregeld. Hoewel we ook vinden dat het <tt>deb</tt>-formaat een
    aantal voordelen heeft ten opzichte van het <tt>rpm</tt>-formaat, is
    het deze integratie tussen de pakketten die Debian zeer robuust
    maakt.</dd>

<dt><strong>Broncode</strong></dt>

    <dd>Als u een software-ontwikkelaar bent, zult u het vast kunnen
    waarderen dat er honderden ontwikkelingstools en talen, plus
    miljoenen regels broncode in het basissysteem zijn opgenomen. Alle
    software in de hoofddistributie voldoet aan de regels van de <a
    href="$(HOME)/social_contract#guidelines">Debian Richtlijnen voor Vrije
    Software (DFSG)</a>. Dit betekent dat u al deze code vrijelijk mag
    gebruiken om te bestuderen en op te nemen in nieuwe vrije-software
    projecten. Er zijn ook meer dan genoeg gereedschappen en broncode
    aanwezig die gebruikt kunnen worden voor gepatenteerde
    projecten.</dd>

<dt><strong>Gemakkelijke upgrades</strong></dt>

    <dd>Door het pakketsysteem is het opwaarderen naar een
    nieuwe versie van Debian kinderspel. Voer gewoon de commando's
    <tt>apt-get update; apt-get dist-upgrade</tt> (of
    <tt>aptitude update; aptitude dist-upgrade</tt> in een recentere release)
    uit en u kunt opwaarderen vanaf een cd of via het internet vanaf een
    van de meer dan 300 <a href="$(HOME)/mirror/list">spiegelservers</a>.</dd>

<dt><strong>Diversiteit in platforms en kernels</strong></dt>

    <dd>Debian ondersteunt momenteel een indrukwekkend aantal CPU-architecturen:
    alpha, amd64, armel, hppa, i386, ia64, mips, mipsel, powerpc, s390 en
    sparc. En naast Linux draait Debian ook op GNU&nbsp;Hurd- en FreeBSD
    kernels. Dank zij het hulpprogramma debootstrap zal u moeite moeten doen om
    een apparaat te vinden waarop Debian niet werkt.</dd>

<dt><strong>Het bugvolgsysteem</strong></dt>

	<dd>Debian’s <a href="https://bugs.debian.org/">bugvolgsysteem</a> is
	publiek beschikbaar. We proberen niet te verbergen dat software niet altijd
	werkt zoals de gebruiker wil. We stimuleren gebruikers om bugrapporten in
	te sturen en zij krijgen bericht wanneer en hoe de bug is opgelost. Dit
	systeem zorgt ervoor dat Debian snel en eerlijk op problemen kan
	reageren.</dd>

</dl>

<p>Als u nog geen Debian-gebruiker bent, dan zullen de volgende voordelen
u vast bevallen:</p>
<dl>

<dt><strong>Stabiliteit</strong></dt>

    <dd>Er zijn vele voorbeelden van machines die meer dan een jaar
    continu draaien zonder te herstarten. Zelfs dan worden ze alleen
    herstart na een stroomstoring of een hardware upgrade. Vergelijk
    dat eens met andere systemen die soms meerdere malen per dag
    crashen.</dd>

<dt><strong>Snel en laag geheugengebruik</strong></dt>

	<dd>Andere besturingssystemen zijn vaak snel in een of twee specifieke
	gebieden, maar omdat Debian gebaseerd is op GNU/Linux of GNU/kFreeBSD, is
	het snel en flexibel. Windows-programmatuur draait onder een emulator in
	GNU/Linux soms zelfs <strong>sneller</strong> dan in de oorspronkelijke
	omgeving.</dd>

<dt><strong>De meeste stuurprogramma's voor hardware worden geschreven door
gebruikers van GNU/Linux / GNU/kFreeBSD, niet door de producenten</strong></DT>

    <dd>Hoewel dit betekent dat er soms wat vertraging optreedt voordat
    nieuwe hardware wordt ondersteund en dat voor sommige hardware
    ondersteuning ontbreekt, maakt het ondersteuning van
    hardware mogelijk lang nadat de producent gestopt is met de
    productie ervan of failliet is gegaan. De ervaring leert dat Open
    Source stuurprogramma's meestal veel beter zijn dan gepatenteerde
    stuurprogramma's.</dd>

<dt><strong>Goede systeembeveiliging</strong></dt>

    <dd>Debian en de vrijesoftwaregemeenschap zijn erg alert op
    beveiligingsproblemen en streven ernaar deze zo snel mogelijk te verhelpen
    in de distributie. Veelal komen verbeterde pakketten binnen enkele dagen
    beschikbaar. De beschikbaarheid van broncode maakt het mogelijk de
    beveiliging in Debian in een open context te evalueren, hetgeen de
    implementatie van slechte beveiligingsmodellen voorkomt. Daarnaast maken
    de meeste vrijesoftwareprojecten gebruik van peerreviewsystemen,
    hetgeen al vooraf helpt te voorkomen dat potentiële
    beveiligingsproblemen worden geïntroduceerd in essentiële
    systemen.</dd>

<dt><strong>Beveiligingsprogrammatuur</strong></dt>

    <dd>Hoewel velen dit niet beseffen, kan alles wat over het
    net wordt verstuurd, worden gelezen door elke machine die zich
    tussen u en de ontvanger bevindt. Debian bevat pakketten van de
    beroemde GPG (en PGP) programma’s waarmee e-mail versleuteld tussen
    gebruikers kan worden verstuurd. Bovendien geeft ssh u de
    mogelijkheid om een veilige verbinding te realiseren met een andere
    machine waarop ssh geïnstalleerd is.</dd>

</dl>

<p>Natuurlijk is Debian niet perfect. Er zijn drie gebieden waarover
regelmatig wordt geklaagd:</p>
<dl>

<dt><em><q>Gebrek aan populaire commerciële programmatuur.</q></em></dt>

    <dd><p>Het is natuurlijk helemaal waar dat enkele populaire programma’s
    niet beschikbaar zijn voor Linux. Er zijn echter
    alternatieven voor de meeste hiervan. Deze vervangende programma’s
    worden ontwikkeld om de
    beste eigenschappen van de gepatenteerde programma’s na te bootsen,
    met het extra voordeel dat het <a href="free">vrije software</a> is.</p>

    <p>Een gebrek aan kantoorsoftware zoals Word en Excel is niet langer
    een probleem, omdat Debian drie officepakketten bevat die volledig
    <a href="free">vrije software</a> zijn:
    <a href="http://www.libreoffice.org/">LibreOffice</a>,
    <a href="https://www.calligra.org/">Calligra</a> en
    <a href="https://wiki.gnome.org/Projects/GnomeOffice">GNOME office applicaties</a>.</p>

    <p>Ook zijn er verschillende gepatenteerde officepakketten beschikbaar:
    <a href="http://vistasource.com/index.php?id=applixware">Applixware
    (Anyware)</a>,
    <a href="http://www.hancom.com/">Hancom Office</a> en andere.</p>

    <p>Voor degenen die zijn geïnteresseerd in databanken, komt
    Debian met twee populaire databasepakketten:
    <a href="https://www.mysql.com/">MySQL</a> en
    <a href="https://www.postgresql.org/">PostgreSQL</a>.
    <a href="https://www.sap.com/community/topic/maxdb.html">SAP DB</a>,

    <a href="https://www.ibm.com/analytics/us/en/technology/informix/">Informix</a>,
    <a href="https://www.ibm.com/analytics/us/en/technology/db2/db2-linux-unix-windows.html">IBM DB2</a>
    en andere zijn ook beschikbaar voor GNU/Linux.</p>


	<p>Ook wordt heel wat andere gepatenteerde software in grote aantallen
    uitgebracht nu meer bedrijven de kracht van GNU/Linux / GNU/kFreeBSD en
    de grotendeels onaangeboorde markt van het snel groeiend aantal gebruikers
	ontdekken. (Omdat GNU/Linux and GNU/kFreeBSD vrij verspreid kunnen
    worden, kunnen geen verkoopcijfers gebruikt worden om het aantal
    gebruikers in te schatten. De beste schatting is dat GNU/Linux een
	marktaandeel heeft van 5%, dat overeenkomt met 15 miljoen mensen in het
	begin van 2001).</p>
    </dd>

<dt><em><q>Debian is moeilijk te configureren.</q></em></dt>

    <dd>Merk op dat hier staat "configureren", niet "installeren", omdat
    sommige mensen de eerste installatie van Debian gemakkelijker vinden
    dan die van Windows. Voor veel hardware (zoals bijvoorbeeld
    printers) zou het instellen ervan gemakkelijker kunnen worden gemaakt.
    Ook zou sommige software eigenlijk een script moeten
    hebben dat de gebruiker stap voor stap door het configuratieproces
    leidt (in ieder geval voor de meest voorkomende instellingen). Hier
    wordt aan gewerkt.</dd>

<dt><em><q>Niet alle hardware wordt ondersteund.</q></em></dt>

    <dd>Dit slaat met name op heel erg nieuwe, erg oude en erg zeldzame
    hardware. Ook voor hardware die afhankelijk is van gecompliceerde
    <q>stuurprogramma</q>software die de fabrikant enkel voor het
    Windowsplatform levert (zoals bijvoorbeeld softwaremodems of
    wifi-apparaten voor de laptop), gaat
    dit op. Echter, in de meeste gevallen, is er equivalente hardware
    beschikbaar die wel werkt met Debian. Sommige hardware wordt niet
    ondersteund omdat de producent de specificaties ervan niet bekend wil
    maken. Ook hier wordt aan gewerkt.</dd>
</dl>

<p>Als het bovenstaande niet voldoende is om u te overtuigen Debian te
gaan gebruiken, denk dan eens over het volgende: lage kosten
(enkel de prijs van een netwerkverbinding),
gemakkelijke installatie en echte multitasking die gemakkelijk uw
productiviteit kan verdubbelen. Hoe kunt u zich veroorloven om het niet
eens te proberen?</p>

# <P>Related links:
# http://alexsh.hectic.net/debian.html
