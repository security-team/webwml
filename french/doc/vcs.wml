#use wml::debian::ddp title="Le dépôt VCS du projet de documentation Debian"
#use wml::debian::toc
#use wml::debian::translation-check translation="bf1a0486dfbf35e11a7ff98a29d9e2e4f2eda3f3" maintainer="Jean-Paul Guillonneau"

# Translator:
# Mickael Simon, 2001-2003
# Frédéric Bothamy, 2005, 2007.
# Simon Paillard, 2008, 2009.
# David Prévot, 2010.
# Jean-Paul Guillonneau, 2018-2020
<p>Les pages web et la plupart des manuels du Projet de documentation Debian
sont disponibles sur le serveur Salsa de Debian à l'adresse
<strong>salsa.debian.org</strong> qui est l’instance GitLab de Debian.
Veuillez lire la <a href="https://wiki.debian.org/Salsa">documentation sur
Salsa</a> pour davantage d’informations sur le fonctionnement de ce service.
</p>

<p>Tout le monde peut télécharger les sources à partir du service Salsa. Seuls
les membres du projet de documentation Debian peuvent les mettre à jour.

<toc-display />

<toc-add-entry name="access">Accéder aux sources sur le dépôt Git</toc-add-entry>

<p>Vous pouvez utiliser l'interface web pour accéder aux différents fichiers et
voir les modifications dans chaque projet sur
<url "https://salsa.debian.org/ddp-team/" />.</p>


<p>Pour télécharger un manuel complet, un accès direct au serveur Git
est souvent la meilleure solution. Vous aurez besoin du paquet <tt><a
href="https://packages.debian.org/git">git</a></tt> sur votre propre
machine.</p>

<h3>Cloner anonymement un dépôt Git (mode lecture seule)</h3>

<p>Utilisez cette commande pour télécharger tous les fichiers d’un seul projet&nbsp;:</p>

<p style="margin-left: 2em">
  <code>git clone https://salsa.debian.org/ddp-team/release-notes.git</code>
</p>

<p>Faites de même pour chaque projet que vous voulez cloner localement.</p>

<h3>Cloner un dépôt Git avec les privilèges de mise à jour (mode lecture et écriture)</h3>

<p>Avant d’accéder au serveur Git en utilisant cette méthode, les droits
d’écriture doivent d’abord vous être accordés. Veuillez lire en premier comment
<a href="#obtaining">solliciter</a> la permission de mise à jour.</p>

<p>Utilisez cette commande pour télécharger tous les fichiers d’un projet
particulier :</p>

<p style="margin-left: 2em">
  <code>git clone git@salsa.debian.org:ddp-team/release-notes.git</code>
</p>

<p>Faites de même pour chaque projet que vous voulez cloner localement.</p>

<h3>Récupérer les modifications du dépôt distant de Git</h3>

<p>Pour actualiser votre copie locale avec les modifications effectuées par
d'autres personnes, entrez dans le répertoire
<strong>manuals</strong> et lancez la commande&nbsp;:</p>

<p style="margin-left: 2em">
  <code>git pull</code>
</p>

<toc-add-entry name="obtaining">Obtenir les privilèges de mise à jour</toc-add-entry>

<p>Les privilèges de mise à jour (push) sont accordés à tous ceux désirant
participer à l’écriture des manuels. Nous demandons habituellement que vous
ayez d’abord présenté quelques correctifs utiles.</p>

<p>Après la création de votre compte pour <a href="https://salsa.debian.org/">Salsa</a>,
demandez les privilèges de mise à jour en cliquant sur <q>Request to join</q>
pour le groupe ou n’importe quel projet particulier dans
<url "https://salsa.debian.org/ddp-team/" />.
Veuillez ensuite envoyer un courriel à debian-doc@lists.debian.org expliquant
les antécédents de votre travail dans Debian.</p>

<p>Une fois votre requête acceptée, vous serez membre du groupe <a
href="https://salsa.debian.org/ddp-team/"><q>ddp-team</q> </a> ou d’un de ses
projets.
</p>

<hr />

<toc-add-entry name="updates">Mécanisme de mise à jour automatique</toc-add-entry>

<p>Les pages web publiées de texte de manuel sont générées sur
www-master.debian.org dans le cadre du processus de reconstruction qui se
déroule toutes les quatre heures.</p>

<p>Le processus est réglé pour télécharger les dernières versions des paquets de
l’archive, reconstruire chaque manuel et installer les fichiers dans le
sous-répertoire <code>doc/manuals/</code> du site web.</p>

<p>Les fichiers de la documentation générés par le script de mise à jour sont
disponibles sur <a href="manuals/">https://www.debian.org/doc/manuals/</a>.</p>

<p>Les fichiers des journaux générés par le processus de mise à jour sont
disponibles sur <url "https://www-master.debian.org/build-logs/webwml/" />
(le script s’appelle <code>7doc</code> et est exécuté dans le cadre d’une tâche
cron <code>often</code>).</p>

#<p>Notez que ce processus régénère le répertoire <code>/doc/manuals/</code>.
#Le contenu du répertoire <code>/doc/</code> est généré soit depuis
#<a href="/devel/website/desc">webwml</a> soit depuis d'autres scripts,
#comme ceux qui extraient les manuels de certains paquets.</p>

# <!-- Created: Mon Jul  6 19:58:09 BST 1998 -->
