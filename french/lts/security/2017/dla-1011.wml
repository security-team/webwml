#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les versions 1.8.20p1 de sudo de Todd Miller et précédentes sont vulnérables
à une validation d’entrée (nouvelles lignes incorporées) dans la fonction
get_process_ttyname(), aboutissant à une divulgation d'informations et une
exécution de commande.</p>

<p>L’annonce précédente (DLA-970-1) concernait un problème de sécurité similaire
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000367">CVE-2017-1000367</a>)
qui n’était pas entièrement corrigé.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.8.5p2-1+nmu3+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1011.data"
# $Id: $
