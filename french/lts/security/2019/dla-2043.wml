#use wml::debian::translation-check translation="1e848ced6f2d3ce80d48475db316adbb7b8824f1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes dans gdk-pixbuf, une bibliothèque de gestion de pixbuf,
ont été découverts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6352">CVE-2016-6352</a>

<p>Correction pour un déni de service (écriture hors limites et plantage) à
l’aide de dimensions contrefaites dans un fichier ICO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2870">CVE-2017-2870</a>

<p>Correction pour une vulnérabilité exploitable de dépassement d’entier dans la
fonction tiff_image_parse. Lorsque du logiciel est compilé avec clang, un
fichier tiff contrefait spécialement peut provoquer un dépassement de tas
aboutissant à une exécution de code à distance. Le paquet Debian est compilé
avec gcc et n’est pas touché, mais probablement quelque aval l’est.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6312">CVE-2017-6312</a>

<p>Correction pour un dépassement d'entier dans io-ico.c, qui permet à des
attaquants de provoquer un déni de service (erreur de segmentation et plantage
d'application) à l'aide d'une image contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6313">CVE-2017-6313</a>

<p>Correction pour un dépassement d'entier par le bas dans la fonction
load_resources dans io-icns.c, qui permet des attaquants de provoquer un déni de
service (lecture hors limites et plantage du programme) à l'aide d'une taille
d’image en entrée contrefaite dans un fichier ICO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6314">CVE-2017-6314</a>

<p>Correction pour un boucle infinie dans la fonction make_available_at_least
in io-tiff.c, qui permet à des attaquants de provoquer un déni de service
à l'aide d'un large fichier TIFF.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.31.1-2+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdk-pixbuf.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2043.data"
# $Id: $
