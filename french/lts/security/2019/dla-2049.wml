#use wml::debian::translation-check translation="050065bc1cb4ad0b614d5b85a00f7db9c6a13584" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans imagemagick, une boîte à
outils de traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19948">CVE-2019-19948</a>

<p>Dépassement de tampon basé sur le tas dans WriteSGIImage (coders/sgi.c)
causé par une validation insuffisante de tailles de colonnes et rangées. Cette
vulnérabilité peut être exploitée par des attaquants distants pour provoquer un
déni de service ou tout autre impact non précisé à l’aide de données d’image
contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19949">CVE-2019-19949</a>

<p>Lecture hors limites de tampon basé sur le tas (due à un décalage d'entier)
dans WritePNGImage (coders/png.c) causée par une vérification manquante de
largeur préalable à un déréférencement de pointeur. Cette vulnérabilité peut
être exploitée par des attaquants distants pour provoquer un déni de service à
l’aide de données d’image contrefaites.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8:6.8.9.9-5+deb8u19.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2049.data"
# $Id: $
