#use wml::debian::template title="Négociation du contenu"
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc" translation_maintainer="Frédéric Bothamy"

<H3>Comment le serveur sait quel fichier servir</H3>
<p>Vous aurez noté que les liens internes ne se terminent pas par .html.
C'est parce que le serveur utilise une négociation de contenu pour
décider quelle version du document il doit transmettre. Quand il y a
plus d'une possibilité, le serveur fait une liste de tous les fichiers
possibles à servir, par exemple si la demande est pour «&nbsp;about&nbsp;»,
alors la liste des possibilités peut être about.en.html et
about.de.html. L'action par défaut pour les serveurs Debian est de
servir le document anglais, mais c'est configurable.</p>

<p>Si un client a la bonne variable configurée, par exemple pour servir
de l'allemand, alors, dans l'exemple ci-dessus, about.de.html sera
servi. La chose agréable dans cette configuration est que si la langue
désirée n'est pas disponible, une langue différente est servie à la
place (ce qui, on peut l'espérer, est mieux que rien). La décision de
savoir quel document est servi est un peu déroutante aussi, au lieu de la
décrire ici, vous aurez la réponse complète à
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">https://httpd.apache.org/docs/current/content-negotiation.html</a>
si vous êtes intéressé.</p>

<p>Parce que beaucoup d'utilisateurs ne connaissent même pas
l'existence de la négociation de contenu, il existe des liens au bas de
chaque page pointant directement sur la version de la page pour
chacune des autres langues disponibles. Celles-ci sont déterminées par un script
perl appelé par wml quand la page est générée.</p>

<p>Il existe aussi une option pour contourner les préférences de langue pour le
navigateur en utilisant un cookie donnant la priorité à une langue parmi les
préférées du navigateur.</p>
