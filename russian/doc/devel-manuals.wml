#use wml::debian::ddp title="Руководства для разработчиков Debian"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="78f856f0bff4125a68adb590b66952d97f183227" maintainer="Lev Lamberov"

<document "Руководство по политике Debian" "policy">

<div class="centerblock">
<p>
  Этот документ описывает требования политики к дистрибутиву
  Debian GNU/Linux. Он включает в себя описание структуры и содержания архива Debian,
  замечания по устройству операционной системы, а также технические
  требования, которым должен удовлетворять каждый пакет, включённый в
  дистрибутив.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  готов
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Предлагаемые поправки</a> к Политике

  <p>Документы, дополняющие Политику:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Стандарт иерархии файловых систем</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">простой текст</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Перечень проверок перед обновлением</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Список имён виртуальных пакетов</a>
    <li><a href="packaging-manuals/menu-policy/">Политика меню</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">простой текст</a>]
    <li><a href="packaging-manuals/perl-policy/">Политика Perl</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">простой текст</a>]
    <li><a href="packaging-manuals/debconf_specification.html">Спецификация debconf</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Политика Emacsen</a>
    <li><a href="packaging-manuals/java-policy/">Политика Java</a>
    <li><a href="packaging-manuals/python-policy/">Политика Python</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">Спецификация формата файла copyright</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Справочник разработчика Debian" "devref">

<div class="centerblock">
<p>
  Это руководство описывает процедуры и ресурсы для сопровождающих пакетов
  Debian. Оно объясняет, как стать разработчиком, процедуру загрузки на сервер,
  как работать с системой отслеживания ошибок, списками рассылки, серверами
  Internet и т.д.

  <p>Это руководство позиционируется как <em>справочное руководство</em> для
  всех разработчиков Debian (новичков и умудрённых опытом профессионалов).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  готов
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Руководство для сопровождающих Debian" "debmake-doc">

<div class="centerblock">
<p>
Данное руководство описывает процесс сборки пакета Debian с помощью команды
<code>debmake</code> обычным пользователям Debian и будущим разработчикам.
</p>
<p>
Руководство сконцентрировано на современном стиле создания пакетов и содержит множество простых примеров.
</p>
<ul>
<li>Создание пакетов со сценариями командной строки POSIX</li>
<li>Создание пакетов со сценариями Python3</li>
<li>C с Makefile/Autotools/CMake</li>
<li>Многочисленные двоичные пакеты с разделяемыми библиотеками и т. д.</li>
</ul>
<p>
Данное <q>Руководство для сопровождающих Debian</q> может рассматриваться как продолжение
<q>Руководства нового сопровождающего Debian</q>.
</p>

<doctable>
 <authors "Osamu Aoki">
 <maintainer "Osamu Aoki">
 <status>
 готов
 </status>
 <availability>
 <inpackage "debmake-doc">
 <inddpvcs-debmake-doc>
 </availability>
</doctable>
</div>

<hr>

<document "Руководство нового сопровождающего Debian" "maint-guide">

<div class="centerblock">
<p>
  Этот документ пытается описать создание пакетов Debian GNU/Linux
  для обычного пользователя Debian (и, желательно, разработчика) доступным
  языком и со множеством примеров.

  <p>В отличие от предыдущих попыток, здесь упор сделан на <code>debhelper</code>
  и новые инструменты разработчиков. Автор старается учесть опыт предыдущих
  попыток и усилий.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  будет заменён на "Руководство для сопровождающих Debian" (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Введение в создание пакетов Debian" "packaging-tutorial">

<div class="centerblock">
<p>
Это руководство является введением в пакетирование Debian.
Оно учит будущих
разработчиков как изменять существующие пакеты, как создавать собственные
и как взаимодействовать с сообществом Debian.
В дополнение к основному
руководству в него включены три практических занятия по изменению пакета
<code>grep</code> и пакетированию игры <code>gnujump</code> и библиотеки Java.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  готов
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "Система меню Debian" "menu">

<div class="centerblock">
<p>
  Это руководство описывает системы меню в Debian и пакет <strong>menu</strong>.

  <p>Пакет menu был начат с программы install-fvwm2-menu, поставляемой со
  старым пакетом fvwm2. Однако, пакет menu пытается предоставить более общий
  интерфейс построения меню. Команде update-menus из этого пакета не нужны
  пакеты оконных менеджеров X, она может предоставлять единый интерфейс и для
  тексто- и X-ориентированных программ.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  готов
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML-версия</a>
  </availability>
</doctable>
</div>

<hr>

<document "Внутреннее устройство Debian Installer" "d-i-internals">

<div class="centerblock">
<p>
  Этот документ предназначен для того, чтобы сделать Debian Installer более понятным для
  новых разработчиков, и чтобы служить центром документирования технической информации.

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  готов
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML-версия</a>.</p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">Исходный код в DocBook XML</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "Документация по dbconfig-common" "dbconfig-common">

<div class="centerblock">
<p>
 Документация предназначена для сопровождающих пакетов, которые сопровождают пакеты,
 для которых требуется работающая база данных. Вместо самостоятельной реализации требуемой
 логики можно использовать dbconfig-common с тем, чтобы задать необходимые вопросы во время
 установки, обновления, повторной настройки или удаления пакетов, а также создания и заполнения
 базы данных.

<doctable>
 <authors "Sean Finney, Paul Gevers">
 <maintainer "Paul Gevers">
 <status>
 готов
 </status>
 <availability>
 <inpackage "dbconfig-common">
 <inddpvcs-dbconfig-common>
 Кроме того, доступна <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">документация о разработке</a>.
 </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
 Предложенная политика для пакетов, зависящих от работающей базы данных.

<doctable>
 <authors "Sean Finney">
 <maintainer "Paul Gevers">
 <status>
 черновик
 </status>
 <availability>
 <inpackage "dbconfig-common">
 <inddpvcs-dbapp-policy>
 </availability>
</doctable>
</div>
