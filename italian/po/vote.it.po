# Debian Web Site. templates.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2002.
# Giuseppe Sacco <eppesuig@debian.org>, 2004-2007.
# Luca Monducci <luca.mo@tiscali.it>, 2008-2019.
# 
msgid ""
msgstr ""
"Project-Id-Version: templates.it\n"
"PO-Revision-Date: 2019-04-27 14:26+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Scadenze"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Sommario"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Designazioni"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Revoche"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Discussione"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Programmi"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Proponente"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Proponente della proposta A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Proponente della proposta B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Proponente della proposta C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Proponente della proposta D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Proponente della proposta F"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Proponente della proposta F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Proponente della proposta G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Proponente della proposta H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Sostenitori"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Sostenitori della proposta A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Sostenitori della proposta B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Sostenitori della proposta C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Sostenitori della proposta D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Sostenitori della proposta E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Sostenitori della proposta F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Sostenitori della proposta G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Sostenitori della proposta H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opposizione"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Testo"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Proposta A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Proposta B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Proposta C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Proposta D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Proposta E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Proposta F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Proposta G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Proposta H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Scelte"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Proponente dell'emendamento"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Sostenitori dell'emendamento"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Testo dell'emendamento"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Proponente dell'emendamento A"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Sostenitori dell'emendamento A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Testo dell'emendamento A"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Proponente dell'emendamento B"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Sostenitori dell'emendamento B"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Testo dell'emendamento B"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Proponente dell'emendamento C"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Sostenitori dell'emendamento C"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Testo dell'emendamento C"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Ememndamenti"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Procedimenti"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Maggioranza necessaria"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Dati e statistiche"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Quorum"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Discussione minima"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Voto"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Esito"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "In&nbsp;attesa&nbsp;di&nbsp;sponsor"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "In&nbsp;discussione"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Aperte&nbsp;al&nbsp;voto"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Decise"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Ritirato"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Altro"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Pagina&nbsp;delle&nbsp;votazioni"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Come&nbsp;fare&nbsp;per"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Inviare&nbsp;una&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Approvare&nbsp;una&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Seguire&nbsp;una&nbsp;proposta"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Leggere&nbsp;il&nbsp;risultato"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Votare"
