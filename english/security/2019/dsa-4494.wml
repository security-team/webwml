<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Dominik Penner discovered that KConfig, the KDE configuration settings
framework, supported a feature to define shell command execution in
.desktop files. If a user is provided with a malformed .desktop file
(e.g. if it's embedded into a downloaded archive and it gets opened in
a file browser) arbitrary commands could get executed. This update
removes this feature.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 5.28.0-2+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 5.54.0-1+deb10u1.</p>

<p>We recommend that you upgrade your kconfig packages.</p>

<p>For the detailed security status of kconfig please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/kconfig">\
https://security-tracker.debian.org/tracker/kconfig</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4494.data"
# $Id: $
