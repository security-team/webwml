<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Linux 4.9 has been packaged for Debian 8 as linux-4.9.  This provides
a supported upgrade path for systems that currently use kernel
packages from the "jessie-backports" suite.</p>

<p>However, "apt full-upgrade" will *not* automatically install the
updated kernel packages.  You should explicitly install one of the
following metapackages first, as appropriate for your system:</p>

<ul>
    <li>linux-image-4.9-686</li>
    <li>linux-image-4.9-686-pae</li>
    <li>linux-image-4.9-amd64</li>
    <li>linux-image-4.9-armmp</li>
    <li>linux-image-4.9-armmp-lpae</li>
    <li>linux-image-4.9-marvell</li>
</ul>

<p>For example, if the command "uname -r" currently shows
"4.9.0-0.bpo.6-amd64", you should install linux-image-4.9-amd64.</p>

<p>There is no need to upgrade systems using Linux 3.16, as that kernel
version will also continue to be supported in the LTS period.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in linux-latest-4.9 version 80+deb9u5~deb8u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1424.data"
# $Id: $
