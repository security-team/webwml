<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Kaspersky Lab discovered several vulnerabilities in libvncserver, a C
library to implement VNC server/client functionalities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6307">CVE-2018-6307</a>

    <p>a heap use-after-free vulnerability in the server code of the file
    transfer extension, which can result in remote code execution. This
    attack appears to be exploitable via network connectivity.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>

    <p>contains a heap out-of-bound write vulnerability in the server code
    of the file transfer extension, which can result in remote code
    execution. This attack appears to be exploitable via network
    connectivity.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a>

    <p>multiple heap out-of-bound write vulnerabilities in VNC client code,
    which can result in remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

    <p>heap out-of-bound write vulnerability in a structure in VNC client
    code, which can result in remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

    <p>CWE-835: Infinite Loop vulnerability in VNC client code. The
    vulnerability could allow an attacker to consume an excessive amount
    of resources, such as CPU and RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

    <p>CWE-665: Improper Initialization weaknesses in VNC client code,
    which could allow an attacker to read stack memory and can be abused
    for information disclosure. Combined with another vulnerability, it
    can be used to leak stack memory layout and bypass ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20023">CVE-2018-20023</a>

    <p>Improper Initialization vulnerability in VNC Repeater client code,
    which could allow an attacker to read stack memory and can be abused
    for information disclosure. Combined with another vulnerability, it
    can be used to leak stack memory layout and bypass ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

    <p>a null pointer dereference in VNC client code, which can result in
    DoS.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.9.9+dfsg2-6.1+deb8u4.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1617.data"
# $Id: $
