<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in GlusterFS, a
clustered file system. Buffer overflows and path traversal issues may
lead to information disclosure, denial-of-service or the execution of
arbitrary code.</p>

<p>To resolve the security vulnerabilities the following limitations were
made in GlusterFS:</p>

<ul>
    <li>open,read,write on special files like char and block are no longer
      permitted</li>
    <li>io-stat xlator can dump stat info only to /run/gluster directory</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.5.2-2+deb8u4.</p>

<p>We recommend that you upgrade your glusterfs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1510.data"
# $Id: $
