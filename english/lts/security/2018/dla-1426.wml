<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in CUPS, the Common UNIX Printing
System. These issues have been identified with the following CVE ids:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4180">CVE-2018-4180</a>

     <p>Dan Bastone of Gotham Digital Science discovered that a local
     attacker with access to cupsctl could escalate privileges by setting
     an environment variable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4181">CVE-2018-4181</a>

     <p>Eric Rafaloff and John Dunlap of Gotham Digital Science discovered
     that a local attacker can perform limited reads of arbitrary files
     as root by manipulating cupsd.conf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6553">CVE-2018-6553</a>

    <p>Dan Bastone of Gotham Digital Science discovered that an attacker
    can bypass the AppArmor cupsd sandbox by invoking the dnssd backend
    using an alternate name that has been hard linked to dnssd.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.5-11+deb8u4.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1426.data"
# $Id: $
