<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the otrs2 package that
may lead to unauthorized access, remote code execution and spoofing.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1765">CVE-2020-1765</a>

    <p>An improper control of parameters allows the spoofing of the from
    fields of the following screens: AgentTicketCompose,
    AgentTicketForward, AgentTicketBounce.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1766">CVE-2020-1766</a>

    <p>Due to improper handling of uploaded images it is possible in very
    unlikely and rare conditions to force the agents browser to execute
    malicious javascript from a special crafted SVG file rendered as
    inline jpg file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1767">CVE-2020-1767</a>

    <p>Unauthorized view of drafts, change the text completely and send it
    in the name of draft owner. For the customer it will not be visible
    that the message was sent by another agent.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.3.18-1+deb8u13.</p>

<p>We recommend that you upgrade your otrs2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2079.data"
# $Id: $
