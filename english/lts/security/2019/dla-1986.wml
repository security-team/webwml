<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In haml, when using user input to perform tasks on the server, characters
like < > " ' must be escaped properly. In this case, the ' character was
missed. An attacker can manipulate the input to introduce additional
attributes, potentially executing code.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.0.5-2+deb8u1.</p>

<p>We recommend that you upgrade your ruby-haml packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1986.data"
# $Id: $
