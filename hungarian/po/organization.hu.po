msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2011-01-10 22:46+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "jelenleg"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "tag"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
msgid "Stable Release Manager"
msgstr "Kiadásmenedzserek"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "elnök"

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "assistant"
msgstr "FTP asszisztensek"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "titkár"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Tisztviselők"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Disztribúció"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
#, fuzzy
msgid "Publicity team"
msgstr "Sajtó és nyilvánosság"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Felhasználói támogatás és infrastruktúra"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Vezető"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Szakmai bizottság"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Titkár"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Fejlesztési projektek"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "FTP-archívumok"

#: ../../english/intro/organization.data:105
#, fuzzy
msgid "FTP Masters"
msgstr "főnök"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "FTP asszisztensek"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Kiadásmenedzsment"

#: ../../english/intro/organization.data:128
#, fuzzy
msgid "Release Team"
msgstr "Verzióinformációk"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Minőségbiztosítás"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "A telepítési rendszer karbantartói"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Verzióinformációk"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "CD-image-ek"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Gyártás"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Tesztelés"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Felhasználói támogatás és infrastruktúra"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:171
#, fuzzy
msgid "Buildd administration"
msgstr "Rendszeradminisztráció"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Dokumentáció"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "További munkát igénylő és jövőbeli csomagok"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Portolások"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Különleges konfigurációk"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Laptopok"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Tűzfalak"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Beágyazott rendszerek"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Sajtókapcsolatok"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Weboldalak"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
#, fuzzy
msgid "Debian Women Project"
msgstr "Fejlesztési projektek"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Események"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "Szakmai bizottság"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Partnerségi program"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Hardveradomány-koordinátorok"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Felhasználói támogatás"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Hibakövető rendszer"

#: ../../english/intro/organization.data:414
#, fuzzy
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "A levelezőlisták adminisztrátorai"

#: ../../english/intro/organization.data:422
#, fuzzy
msgid "New Members Front Desk"
msgstr "Új karbantartók felvétele"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Debian accountok kezelése"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Kulcscsomó-karbantartók (PGP és GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Biztonsági csoport"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Konzultánsok oldala"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "CD-terjesztők oldala"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Szabályzat"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Rendszeradminisztráció"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ezen a címen jelentheted be a Debian gépeivel kapcsolatos problémákat, "
"beleértve a jelszóproblémákat, vagy ha egy csomagot szeretnél telepíttetni."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ha hardware-problémáid akadtak a Debian gépeivel, látogasd meg a <a href="
"\"https://db.debian.org/machines.cgi\">Debian-gépek</a> oldalt, ahol "
"megtalálod az egyes gépek adminisztrátorát."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP fejlesztői adatbázis adminisztrátora"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Tükrözések"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "DNS-karbantartó"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Csomagkövető rendszer"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Alioth-felügyelet"

#~ msgid "Individual Packages"
#~ msgstr "Egyedi csomagok"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian gyerekeknek 1-től 99 éves korig"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian az orvosi gyakorlatban és kutatásban"

#~ msgid "Debian for education"
#~ msgstr "Debian az oktatásban"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian az ügyvédi irodákban"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian a fogyatékkal élők számára"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Debian az orvosi gyakorlatban és kutatásban"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian az oktatásban"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "A telepítési rendszer karbantartói"

#~ msgid "Publicity"
#~ msgstr "Sajtó és nyilvánosság"

#, fuzzy
#~| msgid "New Maintainers Front Desk"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Új karbantartók felvétele"

#~ msgid "Vendors"
#~ msgstr "Terjesztők"

#~ msgid "Internal Projects"
#~ msgstr "Belső projektek"

#~ msgid "Installation System for ``stable''"
#~ msgstr "A \"stable\" telepítőrendszere"

#~ msgid "Mailing list"
#~ msgstr "Levelezőlista"

#~ msgid "Installation"
#~ msgstr "Telepítés"

#~ msgid "Delegates"
#~ msgstr "Képviselők"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ez még nem hivatalos belő Debian projekt, de bejelentette az igényét az "
#~ "integrációra."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian multimédia"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux a vállalkozási informatikában"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian a nonprofit cégek számára"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Az univerzális asztali operációs rendszer"

#~ msgid "Accountant"
#~ msgstr "Könyvelő"

#~ msgid "Key Signing Coordination"
#~ msgstr "Kulcsaláírás-koordinátorok"

#~ msgid "Mailing List Archives"
#~ msgstr "Levelezőlista-archívumok"

#~ msgid "Handhelds"
#~ msgstr "Tenyérgépek"

#~ msgid "APT Team"
#~ msgstr "APT-team"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "A ''stable'' kiadásmenedzsere"

#~ msgid "Release Assistants"
#~ msgstr "Kiadás asszisztensek"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Testre szabott Debian disztribúciók"

#, fuzzy
#~| msgid "Security Testing Team"
#~ msgid "Marketing Team"
#~ msgstr "Biztonsági tesztelő csoport"

#~ msgid "Security Audit Project"
#~ msgstr "Biztonsági ellenőrzési projekt"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Biztonsági csoport"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth-felügyelet"
