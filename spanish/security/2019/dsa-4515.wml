#use wml::debian::translation-check translation="9f4968f1d9a97ff25da0334028d0e74ab7c96c32"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8644">CVE-2019-8644</a>

    <p>G. Geshev descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8649">CVE-2019-8649</a>

    <p>Sergei Glazunov descubrió un problema que puede dar lugar a ejecución de scripts entre sitios universal («universal
    cross site scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8658">CVE-2019-8658</a>

    <p>akayn descubrió un problema que puede dar lugar a ejecución de scripts entre sitios universal («universal cross site
    scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8666">CVE-2019-8666</a>

    <p>Zongming Wang y Zhe Jin descubrieron problemas de corrupción de memoria que
    pueden dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8669">CVE-2019-8669</a>

    <p>akayn descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8671">CVE-2019-8671</a>

    <p>Apple descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8672">CVE-2019-8672</a>

    <p>Samuel Gross descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8673">CVE-2019-8673</a>

    <p>Soyeon Park y Wen Xu descubrieron problemas de corrupción de memoria que
    pueden dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8676">CVE-2019-8676</a>

    <p>Soyeon Park y Wen Xu descubrieron problemas de corrupción de memoria que
    pueden dar lugar a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8677">CVE-2019-8677</a>

    <p>Jihui Lu descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8678">CVE-2019-8678</a>

    <p>Un investigador anónimo, Anthony Lai, Ken Wong, Jeonghoon Shin,
    Johnny Yu, Chris Chan, Phil Mok, Alan Ho y Byron Wai descubrieron
    problemas de corrupción de memoria que pueden dar lugar a ejecución de código
    arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8679">CVE-2019-8679</a>

    <p>Jihui Lu descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8680">CVE-2019-8680</a>

    <p>Jihui Lu descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8681">CVE-2019-8681</a>

    <p>G. Geshev descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8683">CVE-2019-8683</a>

    <p>lokihardt descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8684">CVE-2019-8684</a>

    <p>lokihardt descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8686">CVE-2019-8686</a>

    <p>G. Geshev descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8687">CVE-2019-8687</a>

    <p>Apple descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8688">CVE-2019-8688</a>

    <p>Insu Yun descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8689">CVE-2019-8689</a>

    <p>lokihardt descubrió problemas de corrupción de memoria que pueden dar lugar a
    ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8690">CVE-2019-8690</a>

    <p>Sergei Glazunov descubrió un problema que puede dar lugar a ejecución de scripts entre sitios universal («universal
    cross site scripting»).</p></li>

</ul>

<p>Puede obtener más detalles en el aviso de seguridad de WebKitGTK y WPE
WebKit WSA-2019-0004.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.24.4-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4515.data"
