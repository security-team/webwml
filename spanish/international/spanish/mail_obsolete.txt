Hola,

La traducción al español de una página del sitio web de Debian
es más antigua que la versión original en inglés.

Si mantiene esta página, por favor actualícela.
Si no mantiene esta página pero desea actualizarla,
por favor informe a la lista debian-l10n-spanish@lists.debian.org.


